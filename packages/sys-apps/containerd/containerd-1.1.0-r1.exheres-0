# Copyright 2016 Marc-Antoine Perennou <marc-antoine.perennou@clever-cloud.com>
# Copyright 2017 Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A daemon to control runC"
DESCRIPTION="
containerd is a daemon to control runC, built for performance and density. containerd
leverages runC's advanced features such as seccomp and user namespace support as well as
checkpoint and restore for cloning and live migration of containers.
"
HOMEPAGE="https://containerd.io"
BUGS_TO="marc-antoine.perennou@clever-cloud.com"
LICENCES="Apache-2.0"

SYSTEMD_SERVICE="containerd.service"

require systemd-service [ systemd_files=[ ${SYSTEMD_SERVICE} ] ] github [ tag=v${PV} ]

SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
    build:
        dev-lang/go
    build+run:
        sys-libs/libseccomp
        sys-fs/btrfs-progs
    run:
        sys-apps/runc
"

# Tests try to run golint and install stuff into /usr/local
# Do not strip Go binaries
RESTRICT="strip test"

GOWORK=src/github.com/${PN}

containerd_make() {
    # make test also make bins again
    # so we need the same exports
    pushd ${GOWORK}/${PN}
    LDFLAGS= GOPATH=$(pwd) emake "$@" GIT_COMMIT=${PV}
    popd
}

src_prepare() {
    default

    edo mkdir -p ${GOWORK}
    edo ln -s ${WORK} ${GOWORK}/${PN}
    edo sed -i 's/local/host/' ${SYSTEMD_SERVICE}
    edo sed -i 's/gofmt.*/true/;s/golint.*/true/' Makefile # fix error: "tee: /dev/stderr: Permission denied"
}

src_compile() {
    containerd_make
}

src_test() {
    containerd_make test
}

src_install() {
    dobin bin/*
    install_systemd_files
}
